const mongoose = require('mongoose');   // import mongoose
const mongooseDelete = require('mongoose-delete');  // import mongoose-delete

const customerSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
        },
        image: {
            type: String,
            required: false,
            get: getImage
        },
    },
    {
        // enable timestamps
        timestamps: {
            createdAt: 'createdAt',
            updatedAt: 'updatedAt',
        },
        toJSON: { getters: true },  // enable getters
    }
);

// getter function image
function getImage(image) {
    if (!image) {
        return null;
    }

    return `/images/${image}`;
}

// enable soft delete
customerSchema.plugin(mongooseDelete, { overrideMethods: 'all' });

module.exports = mongoose.model('customer', customerSchema);    // export good models

