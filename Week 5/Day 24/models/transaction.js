const mongoose = require('mongoose');   // import mongoose
const mongooseDelete = require('mongoose-delete');  // import mongoose-delete

const transactionSchema = new mongoose.Schema(
    {
        customer: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'customer',
        },
        good: {
            type: mongoose.Schema.Types.Mixed,
            required: true,
        },
        quantity: {
            type: Number,
            required: true,
        },
        total: {
            type: Number,
            required: true,
        }
    },
    {
        // enable timestamps
        timestamps: {
            createdAt: 'createdAt',
            updatedAt: 'updatedAt',
        },
    }
);

// enable soft delete
transactionSchema.plugin(mongooseDelete, { overrideMethods: 'all' });

module.exports = mongoose.model('transaction', transactionSchema);    // export good models

