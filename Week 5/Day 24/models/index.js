require('dotenv').config({ path: `.env.${process.env.NODE_ENV}` });     // define env
const mongoose = require('mongoose');   // import mongoose

const URI = process.env.MONGO_URI;      // import URI from env (MONGO_URI)

// connect to MongoDB
mongoose
    .connect(URI)
    .then(() => console.log('Connect to MongoDB'))
    .catch((err) => console.log(err));

exports.customer = require('./customer');
exports.good = require('./good');
exports.supplier = require('./supplier');
exports.transaction = require('./transaction');