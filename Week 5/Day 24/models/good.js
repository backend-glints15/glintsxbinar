const mongoose = require('mongoose');   // import mongoose
const mongooseDelete = require('mongoose-delete');  // import mongoose-delete

const goodSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
            unique: true,
        },
        price: {
            type: Number,
            required: true,
        },
        id_supplier: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'supplier',
        },
        image: {
            type: String,
            required: false,
            get: getImage
        },
    },
    {
        // enable timestamps
        timestamps: {
            createdAt: 'createdAt',
            updatedAt: 'updatedAt',
        },
        toJSON: { getters: true },  // enable getters
    }
);

// getter function image
function getImage(image) {
    if (!image) {
        return null;
    }

    return `/images/${image}`;
}

// enable soft delete
goodSchema.plugin(mongooseDelete, { overrideMethods: 'all' });

module.exports = mongoose.model('good', goodSchema);    // export good models

