require('dotenv').config({ path: `.env.${process.env.NODE_ENV}` });  // enable env
const express = require('express');     // import express
const fileUpload = require('express-fileupload');   // import fileupload

// import routes
const transactions = require('./routes/transactions');

// import errorHandler
const errorHandler = require('./middlewares/errorHandler');

const app = express();  // make express app

// enable req.body (JSON and URL encoded)
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// enable req.body (form-data)
app.use(fileUpload());

// make public to be a static folder
app.use(express.static('public'));

// make routes
app.use('/transactions', transactions);

// if routes not exists
app.all('*', (req, res, next) => {
    next({ statusCode: 404, message: 'Endpoint is not found' });
});

// enable errorHandler
app.use(errorHandler);

// run the server
const port = process.env.port || 3000;
app.listen(port, () => console.log(`Server running on port ${port}...`));
