require('dotenv').config({ path: `.env.${process.env.NODE_ENV}` }); // use dotenv 
const express = require('express'); // import express
const transactions = require('./routes/transactions');

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/transactions', transactions);

const port = process.env.port || 3000;
app.listen(port, () => console.log(`Server running on port ${port}...`));


