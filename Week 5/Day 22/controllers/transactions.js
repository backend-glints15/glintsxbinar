const connection = require('../models');

class Transaction {
    async createTransaction(req, res, next) {
        try {
            const dbConnection = connection.db('sales_morning');    // connect to database sales_morning
            const transaction = dbConnection.collection('transactions');    // connect to table/collection transaction
            const data = await transaction.insertOne(req.body);     // create data in mongoDB

            res.status(201).json({ data });

        } catch (error) {
            console.log(error);
            res.status(500).json({ errors: 'Internal Server Error' });
        }
    }
}

module.exports = new Transaction();