const { MongoClient } = require('mongodb');

// Connection URL
const url = process.env.MONGO_URI;
const client = new MongoClient(url);

// Database Name
const dbName = 'sales_morning';

async function main() {
    try {
        // Use connect method to connect to the server
        await client.connect();
        return 'Connected successfully to server';
    } catch (error) {
        console.log(error);
    }
}

main()
    .then(console.log)
    .catch(console.error);

module.exports = client;