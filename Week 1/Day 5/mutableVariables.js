/* let variable */
let a = 10;
console.log(a);

a = 20;
console.log(a);

/* var variable */
var b = 5;
console.log(b);
var b = 10;
console.log(b);
