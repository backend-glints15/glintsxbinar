const a = 11;
let b = 22;
var c = 33;
let firstName = "Falah";
let lastName = "Izzaty";

// Addition
console.log(firstName + " " + lastName + ": " + (a + b + c));

// Subtraction
console.log(c - b);

// Multiplication
console.log(a * b);

// Division
console.log(c / a);

// Exponential
console.log(a ** 3);
