// Rumus Volume Limas Segi Empat
function limasSegiEmpat(p, l, t) {
  // Luas Alas Limas Segi Empat (alas persegi panjang)
  const luasAlas = p * l;
  const volume = luasAlas * t;

  console.log("Return: " + volume);
  return volume;
}

console.log("--- Limas Segi-4 ---");
let limasSatu = limasSegiEmpat(5, 5, 10);
let limasDua = limasSegiEmpat(4, 4, 8);
console.log("Limas Satu + Limas Dua: " + (limasSatu + limasDua));

// Rumus Volume Kerucut
function kerucut(r, t) {
  const phi = 3.14;
  const volume = (1 / 3) * phi * r ** 2 * t;

  console.log("Return: " + volume);
  return volume;
}

console.log("\n--- Kerucut ---");
let kerucutSatu = kerucut(7, 10);
let kerucutDua = kerucut(10, 15);
console.log("Kerucut Satu + Kerucut Dua: " + (kerucutSatu + kerucutDua));
