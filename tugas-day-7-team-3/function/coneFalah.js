const index = require("../index");

// Formula
function cone(radius, height) {
    const volume = (1 / 3) * Math.PI * radius ** 2 * height;
    return volume;
}

function inputCone() {
    index.rl.question("Radius (Cm): ", (radius) => {
        index.rl.question("Height (Cm): ", (height) => {
            if (!isNaN(radius) && !isNaN(height)) {
                console.log(`\nCone's Volume: ${cone(radius, height)} cm³ \n`);
                index.rl.close();
            } else {
                console.log(`Radius and Height must be a number\n`);
                inputCone();
            }
        });
    });
}

module.exports = { inputCone };