const index = require('../index');

function tube(radius, height) {
    return Math.PI * Math.pow(radius, 2) * height;
}

function inputRadius() {
    index.rl.question(`Radius(cm): `, (radius) => {
      if (!isNaN(radius)) {
        inputHeight(radius)
      } else {
        console.log(`Radius must be a number\n`);
        inputRadius();
      }
    });
  }
  
  function inputHeight(radius) {
    index.rl.question(`Height(cm): `, (height) => {
      if (!isNaN(height)) {
        console.log(`\nTube's Volume: ${tube(radius, height)} cm³`);
        index.rl.close();
      } else {
        console.log(`Height must be a number\n`);
        inputHeight(radius);
      }
    });
  }

module.exports = { inputRadius };
