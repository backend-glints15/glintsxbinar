const { transaction } = require('../models');

class Transaction {
    async createTransaction(req, res, next) {
        try {
            // create data
            // req.body = { id_customer, id_good, quantity, total }, it will be geteing values from client and from validator
            const createdData = await transaction.create(req.body)

            const data = await transaction.findOne({
                where: { id: createdData.id },
                // join with customer and good
                include: [
                    { model: customer },
                    { model: good, include: [{ model: supplier }] },
                ],
            });

            res.status(201).json({ data });

        } catch (error) {
            // console.log(error);
            res.status(500).json({ errors: 'Internal Server Error' });
        }
    }
}

module.exports = new Transaction();