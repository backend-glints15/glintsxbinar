const express = require('express'); // import express
const fileUpload = require('express-fileupload');

// import routes
const transactions = require('./routes/transactions');

const app = express();

// enable req.body (json and urlencoded)
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Enable req.body (form-data)
app.use(fileUpload());

// make routes
app.use('/transactions', transactions);

// run the server
const port = process.env.port || 3000;  // define port
app.listen(port, () => console.log(`Server running on port ${port}... You can do it!`));