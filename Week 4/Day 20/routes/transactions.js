const express = require('express');
const router = express.Router();

// import validator
const { createTransactionsValidator } = require('../middlewares/validators/transactions');

// import controllers
const { createTransaction } = require('../controllers/transactions');

/* routes */
router.post('/', createTransactionsValidator, createTransaction);

// export router
module.exports = router;