const validator = require('validator');
const { good } = require('../../models');

exports.createTransactionsValidator = async (req, res, next) => {
    try {
        const errors = [];

        /* it will check every req.body */
        if (!validator.isNumeric(req.body.id_good)) {
            errors.push(`ID good must be a number!`);
        }
        if (!validator.isNumeric(req.body.id_customer)) {
            errors.push(`ID customer must be number!`);
        }
        if (!validator.isNumeric(req.body.quantity)) {
            errors.push(`Quantity must be a number!`);
        }
        if (errors.length > 0) {
            return res.status(400).json({ errors: errors })
        }

        /* find good, price and total */
        const findGood = await good.findOne({ where: { id: req.body.id_good } });
        // console.log(findGood);

        if (!findGood) {
            errors.push('Good is not exists');
        }
        if (errors.length > 0) {
            return res.status(400).json({ errors: errors })
        }

        const { price } = findGood; // or findGood.price
        req.body.total = parseFloat(price) * parseFloat(req.body.quantity);
        // console.log(total);

        next();

    } catch (error) {
        // console.log(error);
        res.status(400).json({ errors: 'Bad Request' });
    }
}