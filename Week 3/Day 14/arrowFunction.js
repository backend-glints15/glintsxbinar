/* normal function */
function normalFunc(a, b) {
    return a + b;
}

console.log(normalFunc(10, 11));
// mocule exports = { normalFunc}; // exports func

/* arrow function */
const arrowFunc = (a, b) => a + b;
console.log(arrowFunc(12, 13));
// exports.arrowFunc = (a,b) => a + b; // exports directly

/* currying function */
const curryFunc = (a) => (b) => a + b;
console.log(curryFunc(14)(15));
// exports.curryFunc = (a) => (b) => a + b // exports directly