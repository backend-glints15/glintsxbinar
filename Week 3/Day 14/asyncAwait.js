const axios = require('axios');

const fetchApi = async () => {
    let url = 'https://jsonplaceholder.typicode.com/posts';

    try {
        const response = await axios.get(url); // wait to finish

        let data = response.data.filter((item) => item.userId === 5);
        console.log(data);
    } catch (err) {
        console.log(err.message);
    }
}

fetchApi();