const fetch = require('node-fetch');

const storeApi = async () => {
    let apiProducts = 'https://fakestoreapi.com/products';
    let apiCarts = 'https://fakestoreapi.com/carts';
    let apiUsers = 'https://fakestoreapi.com/users';

    let obj = {};

    try {
        const resProds = await fetch(apiProducts);
        const resCarts = await fetch(apiCarts);
        const resUsers = await fetch(apiUsers);

        /* promise */
        // obj = {
        //     products: await resProds.json(),
        //     carts: await resCarts.json(),
        //     users: await resUsers.json(),
        // }

        /* promise all */
        const res = await Promise.all([
            resProds.json(),
            resCarts.json(),
            resUsers.json(),
        ])

        obj = {
            products: res[0],
            carts: res[1],
            users: res[2],
        }

        console.log(obj);

    } catch (err) {
        console.log(err.message);
    }
}

storeApi();