const axios = require('axios');

const storeApi = async () => {
    let apiProducts = 'https://fakestoreapi.com/products';
    let apiCarts = 'https://fakestoreapi.com/carts';
    let apiUsers = 'https://fakestoreapi.com/users';

    let obj = {};

    try {
        /* promise */
        // const resProducts = await axios.get(apiProducts);
        // const resCarts = await axios.get(apiCarts);
        // const resUsers = await axios.get(apiUsers);

        // obj = {
        //     products: resProducts.data,
        //     carts: resCarts.data,
        //     users: resUsers.data,
        // }

        /* promise all */
        const res = await Promise.all([
            axios.get(apiProducts),
            axios.get(apiCarts),
            axios.get(apiUsers),
        ]);

        obj = {
            products: res[0].data,
            carts: res[1].data,
            users: res[2].data,
        }

        console.log(obj);
    } catch (err) {
        console.log(err.message);
    }
}

storeApi();