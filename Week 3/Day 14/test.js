const fetch = require('node-fetch');

let test1 = 'https://reqres.in/api/users?delay=3';
let test2 = 'https://reqres.in/api/users?page=2';
let test3 = 'https://reqres.in/api/unknown';
let data = {};

fetch(test1)
    .then(a => a.json())
    .then(bJson => {
        data = {
            test1: bJson.data.filter(e => e.id < 3)
        }

        return fetch(test2)
    })
    .then(c => c.json())
    .then(cJson => {
        data = {
            ...data,
            c: cJson.data.filter(e => e.id > 7 && e.id < 12)
        }

        return fetch(test3)
    })
    .then(d => d.json())
    .then(dJson => {
        data = {
            ...data,
            d: dJson.data.filter(e => e.id < 4)
        }

        console.log(data);
    })
    .catch(err => console.log(err))