const axios = require('axios');

/* API */
// https://jsonplaceholder.typicode.com/posts
// https://jsonplaceholder.typicode.com/users

axios
    .get('https://jsonplaceholder.typicode.com/posts')
    .then(response => {
        // console.log(response.data);

        // filtering by userId
        let data = response.data;
        data = data.filter(item => item.userId === 5);
        console.log(data);
    })
    .catch((err) => {
        console.log(err.message);
    });
