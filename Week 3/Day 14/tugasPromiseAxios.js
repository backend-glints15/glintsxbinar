const axios = require('axios');

let apiProducts = 'https://fakestoreapi.com/products';
let apiCarts = 'https://fakestoreapi.com/carts';
let apiUsers = 'https://fakestoreapi.com/users';

let obj = {};

axios
    .get(apiProducts)
    .then((res) => {
        obj = { products: res.data };

        return axios.get(apiCarts);
    })
    .then((res) => {
        obj = { ...obj, carts: res.data };

        return axios.get(apiUsers);
    })
    .then((res) => {
        obj = { ...obj, users: res.data };

        console.log(obj);
    })
    .catch((err) => console.log(err.message));