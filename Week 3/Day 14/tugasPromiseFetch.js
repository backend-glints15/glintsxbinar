const fetch = require('node-fetch');
// import fetch from 'node-fetch';

let apiProducts = 'https://fakestoreapi.com/products';
let apiCarts = 'https://fakestoreapi.com/carts';
let apiUsers = 'https://fakestoreapi.com/users';

let obj = {};

fetch(apiProducts)
    .then(p => p.json())
    .then(pJson => {
        obj = { products: pJson }

        return fetch(apiCarts);
    })
    .then(c => c.json())
    .then(cJson => {
        obj = { ...obj, carts: cJson }

        return fetch(apiUsers);
    })
    .then(u => u.json())
    .then(uJson => {
        obj = { ...obj, users: uJson }

        console.log(obj);
    })
    .catch((err) => console.log(err.message));


