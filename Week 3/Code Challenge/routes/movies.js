const express = require('express');
const {
    listMovie,
    getMovieById,
    addMovie,
    updateMovie,
    deleteMovie
} = require('../controller/movies');

const router = express.Router();

router.get('/', listMovie);
router.get('/:id', getMovieById);
router.post('/', addMovie);
router.put('/:id', updateMovie);
router.delete('/:id', deleteMovie);

module.exports = router;