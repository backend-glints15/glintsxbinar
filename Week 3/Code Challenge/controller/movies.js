const data = require('../model/data.json');

class Movie {
    listMovie(req, res, next) {
        try {

            res.status(200).json({ data });

        } catch (error) {
            res.status(500).json({ error: 'Internal Server Error' });
        }
    }

    getMovieById(req, res, next) {
        try {
            const filteredById = data.filter(item => item.id === parseInt(req.params.id));

            if (filteredById.length === 0) {
                return res.status(404).json({ error: 'Movie Not Found!' });
            }

            res.status(200).json({ data: filteredById });

        } catch (error) {
            res.status(500).json({ error: 'Internal Server Error' });
        }
    }

    addMovie(req, res, next) {
        try {
            let setId = data[data.length - 1].id;

            data.push({
                id: setId + 1,
                title: req.body.title,
                genre: req.body.genre,
                year: req.body.year,
            });

            res.status(201).json({ data });

        } catch (error) {
            res.status(500).json({ error: 'Internal Server Error' });
        }
    }

    updateMovie(req, res, next) {
        try {
            const findDataUpdate = data.some(item => item.id === parseInt(req.params.id));

            if (!findDataUpdate) {
                return res.status(404).json({ error: 'Movie Not Found!' });
            }

            const editMovie = data.map(item =>
                item.id === parseInt(req.params.id)
                    ? {
                        id: parseInt(req.params.id),
                        title: req.body.title,
                        genre: req.body.genre,
                        year: req.body.year,
                    }
                    : item
            );

            res.status(200).json({ data: editMovie });

        } catch (error) {
            res.status(500).json({ error: 'Internal Server Error' });
        }
    }

    deleteMovie(req, res, next) {
        try {
            const findDataDelete = data.some(item => item.id === parseInt(req.params.id));

            if (!findDataDelete) {
                return res.status(404).json({ error: 'Movie Not Found!' });
            }

            const deleteMovie = data.filter(item => item.id !== parseInt(req.params.id));

            res.status(200).json({ data: deleteMovie });


        } catch (error) {
            res.status(500).json({ error: 'Internal Server Error' });
        }
    }
}

module.exports = new Movie();