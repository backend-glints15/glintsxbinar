// class
class Person {
    // static property
    static isBreath = true;

    // constructor is and identity of class (instance property)
    constructor(name, address, age) {
        this.name = name;
        this.address = address;
        this.age = age;
    }


    // instance method
    greet() {   // function, class can do something
        return `${this.name}, ${this.age} years old, live in ${this.address}!`;
    }

    // call another method inside class
    greetAndBye() {
        return `${this.greet()} and bye!`;
    }

    // static method
    static run() {
        return `${this.name} is running!`;
    }

    // call another method inside class (static)
    static jogging() {
        return this.run();
    }
}

// add instance method to person
Person.prototype.kick = function () {
    return `${this.name} kick someone!`;
}

// add static method to person
Person.punch = function () {
    return `${this.name} punch someone!`;
}

/* object (instance) */
let dhea = new Person('Dhea', 'Padang', 24);    // name: Dhea, address: Padang, age: 24
let adi = new Person('Adi', 'Situbondo', 25);   // name: Adi, address: Situbondo, age: 25

console.log(adi.name);  // this will be object that you can call it

console.log(dhea.greet());
console.log(adi.greet());
// console.log(Person.greet());   // it will error


/* static */
console.log(Person.isBreath);   // it will work
// console.log(dhea.isBreath);  // it will undefined
console.log(Person.punch());     // it will work
// console.log(adi.run());     // it will error
