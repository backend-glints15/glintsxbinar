const Geometry = require('./geometry');

class ThreeDimention extends Geometry {
    constructor(name) {
        super(name, 'Three Dimention');

        // abstract
        if (this.constructor === ThreeDimention) {
            throw new Error('Cannot initiate from Abstract Class');
        }
    }

    calculateCircumference() {
        console.log(`Calculate ${this._name} circumference!`);
    }

    calculateVolume() {
        console.log(`Calculate ${this._name} volume!`);
    }
}

module.exports = ThreeDimention;

// const test = new ThreeDimention('Bulat');
// console.log(test);