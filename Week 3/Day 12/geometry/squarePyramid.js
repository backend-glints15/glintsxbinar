const ThreeDimention = require('./threeDimention');

class SquarePyramid extends ThreeDimention {
    constructor(length, width, height) {
        super('Square Pyramid');
        this._length = length;
        this._width = width;
        this._height = height;
    }

    calculateCircumference() {
        super.calculateCircumference();

        const area = this._length * this._width;
        return area;
    }

    calculateVolume() {
        super.calculateVolume();

        return this._length * this._width * this._height;
    }
}

module.exports = SquarePyramid;