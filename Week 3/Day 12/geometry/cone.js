const ThreeDimention = require('./threeDimention');

class Cone extends ThreeDimention {
    constructor(radius, height) {
        super('Cone');
        this._radius = radius;
        this._height = height;
    }

    calculateCircumference() {
        super.calculateCircumference();

        let garisPelukis = ((this._radius ** 2) + (this._height ** 2)) ** 1 / 2;
        let baseArea = Math.PI * this._radius * (this._radius + garisPelukis);
        return Math.round(baseArea);
    }

    calculateVolume() {
        super.calculateVolume();

        let volume = Math.PI * this._radius ** 2 * (this._height / 3);
        return Math.round(volume);
    }
}

module.exports = Cone;
