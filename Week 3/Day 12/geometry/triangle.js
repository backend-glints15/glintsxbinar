const TwoDimention = require('./twoDimention');

class Triangle extends TwoDimention {
    constructor(base, height) {
        super('Triangle');
        this._base = base;
        this._height = height;
    }

    // overridding method
    calculateArea() {
        // call parent class that closest
        super.calculateArea();

        return (this._base * this._height) / 2;
    }

    // overloading
    // calculateCircumference(who) {
    // console.log(`Who you are? ${who}`);
    // }

    calculateCircumference() {
        super.calculateCircumference();
        return this._base * 3;
    }
}

module.exports = Triangle;

// let triangleOne = new Triangle(10, 11);
// // console.log(squareOne);
// console.log(triangleOne.calculateArea());
// console.log(triangleOne.calculateCircumference());