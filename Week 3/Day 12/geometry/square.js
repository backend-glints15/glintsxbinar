const TwoDimention = require('./twoDimention');

class Square extends TwoDimention {
    constructor(width) {
        super('square');
        this._width = width;
    }

    // overridding method
    calculateArea() {
        // call parent class that closest
        super.calculateArea();

        return this._width ** 2;
    }

    // overloading
    // calculateCircumference(who) {
    // console.log(`Who you are? ${who}`);
    // }

    calculateCircumference() {
        super.calculateCircumference();
        return this._width * 4;
    }
}

module.exports = Square;

// let squareOne = new Square(10);
// // console.log(squareOne);
// console.log(squareOne.calculateArea());
// console.log(squareOne.calculateCircumference());