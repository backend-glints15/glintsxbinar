const { Rectangle, Square, Triangle, SquarePyramid, Cone } = require('./geometry');

let rectangleOne = new Rectangle(10, 11)
let squareOne = new Square(12)
let triangleOne = new Triangle(13, 14)
let squarePyramidOne = new SquarePyramid(6, 6, 10)
let coneOne = new Cone(7, 10);

// calculate area 2D
let a = rectangleOne.calculateArea();
let b = squareOne.calculateArea();
let c = triangleOne.calculateArea();
let d = a + b + c;
console.log(d);

// calculate circumference 3D
let squarePyramidCircumference = squarePyramidOne.calculateCircumference();
let coneCircumference = coneOne.calculateCircumference();
let totalArea = squarePyramidCircumference + coneCircumference;
console.log(totalArea);

// calculate circumference
let e = rectangleOne.calculateCircumference();
let f = squareOne.calculateCircumference();
let g = triangleOne.calculateCircumference();
let h = e + f + g;
console.log(h);

// calculate volume 3D
let squarePyramidVolume = squarePyramidOne.calculateVolume();
let coneVolume = coneOne.calculateVolume();
let totalVolume = squarePyramidVolume + coneVolume;
console.log(totalVolume);