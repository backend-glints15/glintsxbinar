const express = require('express'); // import express
const app = express();  // make app from express

const students = require('./routes/students');  // import the student routes

app.use(express.json());    // enable read req.body (JSON)
app.use(express.urlencoded({ extended: true, }));   // enable req.body (URL-Encoded)

/* If client go to http://localhost:3000/students or http://localhost:3000 */
app.use('/students', students);

/* running this app */
const port = process.env.PORT || 3000;  // define port
app.listen(port, () => {
    console.log(`Server running on ports ${port}`);
});