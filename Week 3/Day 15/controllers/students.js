const data = require('../models/data.json');

// make student class
class Student {
    getAllStudent(req, res, next) {
        try {
            // it will response to client with status 200 (OK) and {data: data}
            res.status(200).json({ data: data });
        } catch (error) {
            // if something error, it will return response with status 500 and {error: ['internal server error']}
            res.status(500).json({
                error: ['internal server error'],
            });
        }
    }

    getDetailStudent(req, res, next) {
        try {
            // it will filter data by req.params.id
            const detailData = data.filter(item => item.id === parseInt(req.params.id));

            // if no data exists
            if (detailData.length === 0) {
                // it will response client with status 404 and {error: ['student not found']}
                return res.status(404).json({ errors: ['student not found'] });
            }

            // it will response to client with status 200 (OK) and {data: data}
            res.status(200).json({ data: detailData });
        } catch (error) {
            // if something error, it will return response with status 500 and {error: ['internal server error']}
            res.status(500).json({
                errors: ['internal server error'],
            });
        }
    }

    addStudent(req, res, next) {
        try {
            // get the last id of students
            let lastId = data[data.length - 1].id;

            // add data student to data
            data.push({
                id: lastId + 1,
                name: req.body.name,
                nickName: req.body.nickName,
                city: req.body.city,
            });

            // it will response to client with status 201 (created) and {data: data}
            res.status(201).json({ data: data });
        } catch (error) {
            // if something error, it will return response with status 500 and {errors: ['internal server error']}
            res.status(500).json({
                errors: ['internal server error',]
            });
        }
    }

    updateStudent(req, res, next) {
        try {
            // find the data is exist or not
            let findData = data.some(item => item.id === parseInt(req.params.id));

            // if data not exists
            if (!findData) {
                // it will response client with status 404 and {errors: ['student not found']}
                return res.status(404).json({ errors: ['student not found'] });
            }

            // update data student to data by req.params.id
            const editStudent = data.map(item =>
                item.id === parseInt(req.params.id)
                    ? {
                        id: parseInt(req.params.id),
                        name: req.body.name,
                        nickName: req.body.nickName,
                        city: req.body.city,
                    }
                    : item
            );

            // it will response to client with status 201 (created) and {data: data}
            res.status(200).json({ data: editStudent });
        } catch (error) {
            // if something error, it will return response with status 500 and {errors: ['internal server error']}
            res.status(500).json({
                errors: ['internal server error'],
            });
        }
    }

    deleteStudent(req, res, next) {
        try {
            // find the data is exist or not
            let findData = data.some(item => item.id === parseInt(req.params.id));

            // if data not exists
            if (!findData) {
                return res.status(404).json({ errors: ['student not found'] })
            }

            // delete data student to data by req.params.id
            const removeStudent = data.filter(item => item.id !== parseInt(req.params.id));

            // it will response to client with status 201 (created) and {data: data}
            res.status(200).json({ data: removeStudent });
        } catch (error) {
            // if something error, it will return response with status 500 and {errors: ['internal server error']}
            res.status(500).json({
                errors: ['internal server error'],
            });
        }
    }
}

// exports the class
module.exports = new Student();