let people = [
    {
        name: 'Fahmi Alfareza',
        address: ['Magelang, Central Java', 'Malang, East Java'],
        pets: [
            { name: 'Agif', type: 'Cat' },
            { name: 'Hardi', type: 'Cow' },
        ],
        getData: function () {
            console.log(this.name);
            return this.name;
        },
    },
    {
        name: 'Khairul Umam',
        address: ['Medan, North Sumatra', 'Sleman, Yogyakarta'],
        pets: [
            { name: 'Dhea', type: 'Cat' },
            { name: 'Fajar', type: 'Cat' },
        ],
    },
]

console.log(people[0].pets[0]);
console.log(people[0].getData());