let arrObj = [
    {
        name: "John Cena",
        status: "Positive"
    },
    {
        name: "The Rock",
        status: "Suspect"
    },
    {
        name: "Undertaker",
        status: "Negative"
    },
    {
        name: "Kane",
        status: "Positive"
    },
    {
        name: "Big Show",
        status: "Suspect"
    },
    {
        name: "Rey Mysterio",
        status: "Negative"
    },
    {
        name: "Brocklesnar",
        status: "Positive"
    },
    {
        name: "Boogeyman",
        status: "Suspect"
    },
    {
        name: "Matt Hardy",
        status: "Negative"
    },
]


const pos = arrObj.filter(e => e.status.includes('Positive')).map(e => e.name);
const neg = arrObj.filter(e => e.status === 'Negative').map(e => e.name);
const sus = arrObj.filter(e => e.status === 'Suspect').map(e => e.name);

// Random number 1 - 3
let userChoice = Math.floor(Math.random() * 3 + 1);

const switchFunction = () => {
    let retVal = '';
    switch (userChoice) {
        case 1:
            retVal = `${pos} --> Positive`
            break;
        case 2:
            retVal = `${neg} --> Negative`
            break;
        default:
            retVal = `${sus} --> Suspect`
    }
    // console.log(retVal);
    return retVal;
}

// console.log(switchFunction());

exports.switchFunction = switchFunction;



// const pos = []
// const sus = []
// const neg = []

// for (let item of arrObj) {
//     const { name, status } = item;
//     switch (status) {
//         case 'Positive':
//             pos.push(name);
//             break;
//         case 'Negative':
//             neg.push(name);
//             break;
//         case 'Suspect':
//             sus.push(name);
//             break;
//         default:
//             console.log('RIP');
//     }
// }
// console.log(pos);
// console.log(neg);
// console.log(sus);


