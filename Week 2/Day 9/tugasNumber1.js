const arr = ["Tomato", "Broccoli", "Kale", "Cabbage", "Apple"];

for (let i = 0; i < arr.length; i++) {
    if (arr[i] !== 'Apple') {
        console.log(`${arr[i]} is a healthy food, it's definitely worth to eat.`);
    }
}