let person = {
    name: 'Nushair Falah Izzaty',
    address: 'Gedong, Jakarta Timur',
    isMarried: false,
    age: 10
}

// it will print detail of person
console.log(person);

// it will print the name of person
console.log(person.name);
// or
console.log(person['name']);

if (person.isMarried) {
    console.log(`${person.name} has been married`);
} else {
    console.log(`${person.name} has not been married`);
}