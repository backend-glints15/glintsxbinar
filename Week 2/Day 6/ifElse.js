// function getScore(value) {
//     if (value >= 80) {
//         return 'A';
//     } else if (value >= 70) {
//         return 'B';
//     } else if (value >= 60) {
//         return 'C';
//     } else if (value >= 40) {
//         return 'D';
//     } else {
//         return 'E'
//     }
// }

// console.log(getScore(90));

function getScore(value) {
    if (value < 40) return 'F = Zonk!';
    else if (value < 50) return 'E';
    else if (value < 60) return 'D';
    else if (value < 70) return 'C';
    else if (value < 80) return 'B';
    else if (value <= 100) return 'A';
    else return 'Coba lagi!';
}

console.log(getScore(90));