const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// function to calculate Limas Segi Empat
function limasSegiEmpat(p, l, t) {
  // Luas Alas Limas Segi Empat (alas persegi panjang)
  const luasAlas = p * l;
  const volume = luasAlas * t;

  // console.log("Return: " + volume);
  return volume;
}

/* Way 1 */
// function for input panjang of Limas Segi Empat
function inputPanjang() {
  rl.question(`Panjang: `, (panjang) => {
    if (!isNaN(panjang)) {
      inputLebar(panjang);
    } else {
      console.log('Panjang must be a number\n');
      inputPanjang();
    }
  });
}

// function for input lebar of Limas Segi Empat
function inputLebar(panjang) {
  rl.question(`Lebar: `, (lebar) => {
    if (!isNaN(lebar)) {
      inputTinggi(panjang, lebar);
    } else {
      console.log('Lebar must be a number');
      inputLebar(panjang);
    }
  });
}

// function for input tinggi of Limas Segi Empat
function inputTinggi(panjang, lebar) {
  rl.question(`Tinggi: `, (tinggi) => {
    if (!isNaN(tinggi)) {
      const result = `\nLimas Segi Empat: ${limasSegiEmpat(panjang, lebar, tinggi)}`;
      console.log(result);
      rl.close();
    } else {
      console.log('Tinggi must be a number');
      inputTinggi(panjang, lebar);
    }
  });
}
/* End of code Way 1 */


// function to calculate Kerucut
function kerucut(r, t) {
  const phi = 3.14;
  const volume = (1 / 3) * phi * r ** 2 * t;

  // console.log("Return: " + volume);
  return volume;
}

/* Alternative way */
function input() {
  rl.question('Jari-jari: ', (jari) => {
    rl.question('Tinggi: ', (tinggi) => {
      if (jari > 0 && tinggi > 0) {
        console.log(`\nKerucut: ${kerucut(jari, tinggi)}`);
        rl.close();
      } else {
        console.log(`Jari-jari dan Tinggi mush be a number\n`);
        input();
      }
    });
  });
}


// Print way 1
console.log('Way 1: Limas Segi Empat');
inputPanjang();

// Print an alternative way
// console.log('Alternative Way: Limas Segi Empat');
// input();

