let firstName = "Falah";
let lastName = "Izzaty";
let age = 24;

// print full name
console.log(firstName + ' ' + lastName + ', ' + age + ' years old.');

// template literal
let allData = `${firstName} ${lastName}, ${age} years old.`;
console.log(allData);