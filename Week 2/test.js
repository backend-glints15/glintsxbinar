const array = [11, 17, 18, 26, 23, 2345, 113, 71234]

function bubbleSort(array) {
    const jumlahArray = array.length;

    let terdeteksiPerubahan = false;

    for (let i = 0; i < jumlahArray; i++) {

        for (let j = 0; j < jumlahArray - i - 1; j++) {
            // console.log(jumlahArray - i - 1);
            // Ascending >
            // Descending <
            if (array[j] > array[j + 1]) {
                // swap the elements
                const temp = array[j];
                // console.log(temp);
                array[j] = array[j + 1];
                // console.log(array[j]);
                array[j + 1] = temp;
                // console.log(temp);
                // if swapping happens update flag to 1
                terdeteksiPerubahan = true;
            }
        }

        if (!terdeteksiPerubahan) {
            break;
        }

    }

    return array;
}

const filteredArray = array.filter((item) => item != null)

console.log(bubbleSort(filteredArray))
//console.log(filteredArray.sort())