const EventEmitter = require('events')
const readline = require('readline')

// make event instance
const my = new EventEmitter();
// make readline
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

// Listener
my.on('login:failed', function (email) {
    console.log(`${email} is failed to login!`);
    rl.close();
});

my.on('login:success', function (email) {
    console.log(`${email} is success to login!`);
    rl.close();
});

// function to login
function login(email, password) {
    const validatePassword = '123456';

    if (password !== validatePassword) {
        my.emit('login:failed', email); // pass the email to the listener
    } else {
        // do something
        my.emit('login:success', email);
    }
}

// input email and password
rl.question('Email: ', function (email) {
    rl.question('Password: ', function (password) {
        login(email, password);
    });
});