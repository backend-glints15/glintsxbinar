// node processArgv.js daniel yosias doni
const { switchFunction } = require('../Day 9/tugasNumber2');
const EventEmitter = require('events');
const readline = require('readline');

const act = new EventEmitter();
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

act.on('login:success', function (email) {
    console.log(`\n${email} \n\nLogin success...\n`);
    console.log(`${switchFunction()}\n`);
    rl.close();
});

act.on('login:failed', function (email) {
    console.log(`\n${email} Wrong!, try again...\n`);
    rl.close();
});

function login(email, password) {
    // Email & Password
    const validateEmail = 'test@gmail.com';
    const validatePassword = '123456';

    if (email !== validateEmail && password !== validatePassword) {
        act.emit('login:failed', email, password);
    } else {
        act.emit('login:success', email, password);
    }
}

function start() {
    rl.question('Email: ', function (email) {
        rl.question('Password: ', function (password) {
            login(email, password);
        });
    });
}

console.log('\nCheck the code to see email & password ^_^\n');
start();

