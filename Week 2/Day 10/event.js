const EventEmitter = require('events');
// Initialize an instance because events is a class
const my = new EventEmitter();

// Listener
my.on('khairul', function () {
    console.log('Some event happened!');
});

// Emit a listener
my.emit('khairul');